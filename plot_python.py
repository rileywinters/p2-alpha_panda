#This python script will plot provided data

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Import 
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

import sys
import warnings 
import glob 
import numpy as nm 
import matplotlib.pyplot as disp 
import os 
import xlsxwriter 
import time 

#Define var
file_location = 0 
files = 0 
raw_txt_data = []
file_path = 0
wdir = 0

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Section 1, Present Argument And Verify 
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#Display Welcome Line And Push Down Terminal  
print("\n" * 80)
#print('')
print('------------------------------>Welcome To Graph And Spreadsheet Generator Script<-------------------------------')
#print('')
print('________________________________________________________________________________________________________________')

#Take imput from command line and verifiy 
print('')
file_path = input("Please Enter The Name Of The Directory You Wish To Analyze:")

#If File Path Is Not True Then Loop Untill It Is 
while not os.path.exists(file_path):
    file_path = input("Uh Oh, It Seems That The Provided Name Or Path Is Not Valid. Please Try again(No Space Before Name):")

#Change path to provided dir 
os.chdir(file_path)
wdir = os.getcwd()




#Prompt user to confirm path
print('')
#Delay For User 
time.sleep(1)
print('')
print('________________________________________________________________________________________________________________')
print('------------------------------------------------->File Path<----------------------------------------------------')
print('________________________________________________________________________________________________________________')
print("--->" + wdir)
print('________________________________________________________________________________________________________________')
#print('')
print('________________________________________________________________________________________________________________')
print('')
print('Please Verifiy The File Path')
print('')
#Prompt user to confirm path 
confirm = input("Is The Path Displayed Above Correct [y/n]")
print('')

while True: 
    if confirm == 'y':
        break 
    if confirm == 'n':
        print('')
        print('___________________________________________________________________________________________________________________________')
        print('Sorry Please Verifiy That This Script Is In The Parent Directory Of The Folder You Wish To Analyze And Try Again... Exiting')
        print('___________________________________________________________________________________________________________________________')
        print('')
        exit()
    else:
        confirm = input("Please Respond Using [y/n] Only: ")

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#End Section 1
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Section 2 Take Input Files 
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#*** Assume only target files in Dir

#Read all files in given target 
files_list = os.listdir(wdir)
num_of_file = len(files_list)
#Print Input Files 
print('____________________________________________________________________________________________________________________')
print('----------------------------------------------->Input Files<--------------------------------------------------------')
print('____________________________________________________________________________________________________________________')
time.sleep(1)
#print('')
print("\n".join(files_list))
print('____________________________________________________________________________________________________________________')
print('Number Of Input Files = ', num_of_file)
print('____________________________________________________________________________________________________________________')
#print('')

asksep = input("Please Indicate Whether Your File Is A TSV OR CSV (Enter TSV Or CSV): ")

if asksep == "CSV":
    split = "," 

if asksep == "TSV":
    split = "\t"
 
if asksep == "csv":
    split = ","

if asksep == "tsv":
    split = "\t"



#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#End Section 2
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Section 3 Plot Data 
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


time.sleep(1)
#Ask User How They Would Like The Output 
print('___________________________________________________________________________________________________________________')
print('')
print("Output Options")
print('')
print('1: All Output On Same Plot And Save Plot In Current Directory' )
print('')
print('2: Output For Each File Ploted In A Separate Window, Note For This Option You Must Close The Previous Window Before The Next Will Open Plots Will Be Saved In The Current Directory')
print('')
print('3: Do Not Display Plots, Plot Each File On Its Own Plot, And Save In Current Directory')
print('')
print('4: Do Not Dispaly, Plot All Data On One Plot, And Save In Current Directory')
print('')
print('___________________________________________________________________________________________________________________')
print('')
time.sleep(1)
output_option = input("Please Select The Output Option From The List Above And Type The Number Here: ")
print('')
print('___________________________________________________________________________________________________________________')

xunits = input('Please Type The Units For The X Axis Of The Graph :')
print('')
print('___________________________________________________________________________________________________________________')
yunits = input('Please Type The Units For The Y Axis Of The Grpah :')
print('___________________________________________________________________________________________________________________')


while True:

    if output_option == '1':
        disp.figure(figsize=(15,15))
        disp.title("All Data Plotted")
        disp.xlabel(xunits)
        disp.ylabel(yunits)
        for file in files_list:
            data = nm.genfromtxt(file, delimiter=split)
            x = data[:,0]
            y = data[:,1]
            disp.plot(x,y)
        disp.savefig("Plot_Of_All.png")
        disp.show()
        break
 
    if output_option == '2':
        for file in files_list:    
            disp.figure(figsize = (15,15), num=file)
            disp.title(file)
            disp.xlabel(xunits)
            disp.ylabel(yunits)
            data = nm.genfromtxt(file, delimiter= split)
            x = data[:, 0]
            y = data[:, 1]
            disp.plot(x,y)
            disp.savefig(file + ".png")
            disp.show()
        break

    if output_option == '3':
        print('')    
        print('___________________________________________________________________________________________________________________')
        for file in files_list:
            disp.figure(figsize = (15,15), num=file)
            disp.title(file)
            disp.xlabel(xunits)
            disp.ylabel(yunits)
            data = nm.genfromtxt(file, delimiter= split)
            x = data[:, 0]
            y = data[:, 1]
            disp.plot(x,y)
            disp.savefig(file + ".png")
            print("File " + file + "Plotted And Saved In " + wdir)
        print('__________________________________________________________________________________________________________________')    
        print('')
        print("Plotting Completed Succesfully, Files Saved In" + wdir)
        print('')
        print('')
        break 

    if output_option == '4': 
        disp.figure(figsize=(15,15))
        disp.title("All Data Plotted")    
        disp.xlabel(xunits)
        disp.ylabel(yunits)
        for file in files_list:
            data = nm.genfromtxt(file, delimiter= split)
            x = data[:,0]
            y = data[:,1]
            disp.plot(x,y)
        disp.savefig("Plot_Of_All.png")
        print('')
        print("Completed File Located In " + wdir)
        print('')
        break

    else: 
        output_option = input("Please Make A Valid Choice: ")





#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#End Section 3 
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Section 4, Spreadsheet Output
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#Tell User In Terminal 

print('')
print('____________________________________________________________________________________________________________________')
time.sleep(1)
print('Building Xlsx Book')
print('____________________________________________________________________________________________________________________')

#Create Xlsx Workbook
workbook = xlsxwriter.Workbook('Spectra_Data_Table.xlsx',{'nan_inf_to_errors': True} )
#print(files_list)



for item in files_list:  
    sheet=workbook.add_worksheet()
    data = nm.genfromtxt(item, delimiter=split) 
    x = data[:,0]
    y = data[:,1]
    sheet.write(0,0,xunits)
    sheet.write(0,1,yunits)
    sheet.write_column(1,0,x)
    sheet.write_column(1,1,y)
workbook.close()


print('')
print('____________________________________________________________________________________________________________________')
print("Workbook Build Complete, File Located In " + wdir)
print('____________________________________________________________________________________________________________________')
#print('')
#print('')
print('____________________________________________________________________________________________________________________')
print("Run Complete")
print('____________________________________________________________________________________________________________________')
#print('')
#print('')
print('____________________________________________________________________________________________________________________')





#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#End Section 4
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
