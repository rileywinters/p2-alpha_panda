#README  

###Instructions 
Once the script is lanuched in the terminal the following screen prompts should guide the user throughout the script. The first step is to place the script in the parent directory of the folder you wish to analyze and then run the script in the terminal window.The data to be analyzed must be in either comma separated variable form (CSV) or tab separated variable form (TSV). If the data files being analyzed contain additional data, such as instrument information, operator, etc., then the data files must first be prepped with Prep_data.py. This script is self-explanatory, simply run the python script from the terminal and follow the prompts in the terminal window. The first prompt requires the user to input the name of the subdirectory containg the files that they wish to analyze. Here you can enter either "bending" or "spectra". Once the folder name or path is entered, the script will display the path that is loaded and request the user to verify the path. The options for the path will be displayed, so you must type in the # of the option you select. After the directory containing the data is loaded, the script asks the user how they would like the output data and requests that they enter the option of their prefered output format. All output is placed in the folder being analyzed. The script also writes a xlsx worksheet of all of the data being analyzed, with each file being loaded onto its own sheet in the workbook. This sums up most of the important functions of the script.     

###Assumptions  
There are a few major assumptions that need to be satisfied by the user for the script to function properly. The most important one is that the script is placed in parent folder of the sub directory that contains the data being analyzed. If it is not, the user, upon entering the directory name the script will generate an error of file path not found and terminate the script. The major assumptions are listed below.  

1. The script must be placed in the parent directory of the sub directory being analyzed, if not the script will generate an error. 
2. The data being analyzed must be in column format seperated by a single tab space, as this is what the script looks for when spliting lines. 
3. The user should know the name and location of the folder being analyzed, as the name is required for the script and the location is needed as the output is placed there.    

###Known Bugs
-The plot script can not be rerun without first removing the generated files from the given directory. This is due to the script indexing the whole directory. If the script is rerun, it will generate an error when attempting to read the image files.    
-This script requires python 3.0 or greater. It will not run on any version of Python 2.x as some of the input functions have changed since the release of Python 3    

###Expected Output
When running the script, the user should follow the terminal prompts. When selecting the plot output type, the output should match the plotting method described in the prompt. Based on the selected output format, the script should either generate a single plot with all data on a single plot or individual plots that have the file name of the file containg the data in the input file. The output plots will be located in the folder of the input data. The xlsx writer that runs at the end of the script is completly automatic, the output file is placed in the same directory as the input files.
###	Example of Expected Output
	1.
------------------------------>Welcome To Graph And Spreadsheet Generator Script<-------------------------------
________________________________________________________________________________________________________________

Please Enter The Name Of The Directory You Wish To Analyze:

	2. 
------------------------------------------------->File Path<----------------------------------------------------
________________________________________________________________________________________________________________
--->/home/RileyVanhorn/p2-alpha_panda/spectra
________________________________________________________________________________________________________________
________________________________________________________________________________________________________________

Please Verifiy The File Path

Is The Path Displayed Above Correct [y/n]

	3.
____________________________________________________________________________________________________________________
----------------------------------------------->Input Files<--------------------------------------------------------
____________________________________________________________________________________________________________________
Sp15_245L_sect-001_group-2-4_spectrum-H2
Sp15_245L_sect-001_group-2-4_spectrum-H2O
Sp15_245L_sect-001_group-2-4_spectrum-HE
Sp15_245L_sect-001_group-2-4_spectrum-Hg
Sp15_245L_sect-001_group-2-4_spectrum-Ne
____________________________________________________________________________________________________________________
Number Of Input Files =  5
____________________________________________________________________________________________________________________
___________________________________________________________________________________________________________________

Output Options

1: All Output On Same Plot And Save Plot In Current Directory

2: Output For Each File Ploted In A Separate Window, Note For This Option You Must Close The Previous Window Before The Next Will Open, Plots Will Be Saved In The Current Directory

3: Do Not Display Plots, Plot Each File On Its Own Plot, And Save In Current Directory

4: Do Not Dispaly, Plot All Data On One Plot, And Save In Current Directory

___________________________________________________________________________________________________________________

Please Select The Output Option From The List Above And Type The Number Here:

	4.
The completed graph will appear 
