import numpy as nm 
import os
import pandas as pd 
#import break as bk 



#Display Welcome Line And Push Down Terminal  
print("\n" * 10)
print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
#print('')
print('--------------------------------------->Welcome To File Spliter<-------------------------------------------------')
print("-This Script Is Used For Extracting Data From Files That Contain Additional Data To Prep Them For Plot_Python.py-")
#print('')
print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')

#Take imput from command line and verifiy 
print('')
file_path = input("Please Enter The Name Of The Directory Of The Files You Would Like To Prep:")
print('')
print('********************************************************************************************************************')
# If File Path Is Not True Then Loop Untill It Is 
while not os.path.exists(file_path):
    file_path = input("Uh Oh, It Seems That The Provided Name Or Path Is Not Vaild. Please Try again(No Space Before Name):")

#Change path to provided dir 
os.chdir(file_path)
wdir = os.getcwd()

files_list = os.listdir(wdir)

#Ask User For Number Of Lines To SKip 
print('********************************************************************************************************************')
print('')
print("Hint To Grader: For The Data Located In Bending, The Number is 31")
lineskip = input("Please Enter The Number Of Lines Of Additional Data To Be Skipped, Do Not Skip The Column Headers: ")
print('')
print('********************************************************************************************************************')

#Ask For Column Headers 
for file in files_list:
    dr = pd.read_csv(file, skiprows= 31)

print('********************************************************************************************************************')
print(dr.head(0))
print('********************************************************************************************************************')
print('')
print("Please Copy and Paste To Ensure The Header Will Be Correct, If You Get A Key Error, Then The Header Is Incorrect")
print('')
print("Do Not Include Space In Front Of The Headers")
print('')
print('********************************************************************************************************************')
print('')
print(" Dr. Elliott Said He Wanted Stress Vs Strain So The Following Hints Are Provided")
print('')
print('********************************************************************************************************************')
print('')
print("Hint: Enter/Copy & Paste (Strain [Exten.] %) Without ()")
print("")
column1 = input("Please Copy And Paste The Header Of The 1st Column Of Data You Would Like To Pull, This Will Become The X Axis: ")
print('')
print("Hint: Enter/Copy & Paste (Stress MPa) Without ()")
print('')
column2 = input("Please Copy And Paste The Header Of The 2nd Column Of Data You Would Like To Pull, This Will Become The Y Axis: ")
print('')
print('********************************************************************************************************************')

os.mkdir("Converted")


#Create DataFrame from CSV List
#db = Data Block
for file in files_list:
    db = pd.read_csv(file, skiprows= 31)    
    csv1 = db[column1]
    csv2 = db[column2]    
    df = pd.DataFrame({column1 : csv1, column2 : csv2})
    os.chdir("Converted")
    newwdir = os.getcwd()
    df.to_csv(file +".csv", index=False, header=False )
    print('')
    print("File " + file + " Convereted And Saved In " +newwdir )
    os.chdir('..')
print('********************************************************************************************************************')
print("\n" * 4)
print("Split Files Saved In " + newwdir)
print('')
print('********************************************************************************************************************')
print("The Directory Converted Can Now Be Moved To The Main Directory, And Then Analyzed Using Plot_Python Script. ")
print('')
print('')




